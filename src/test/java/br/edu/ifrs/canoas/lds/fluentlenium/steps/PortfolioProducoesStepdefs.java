package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

import java.util.concurrent.TimeUnit;

public class PortfolioProducoesStepdefs extends BaseTest {

    private static final String PORTFOLIO_PRODUCOES = Constants.URL + "portfolio/producoes";


    @Dado("^estou na página do portfólio produções$")
    public void estouNaPaginaDoPortfolioProducoes() {
        goTo(PORTFOLIO_PRODUCOES);
        assertThat(window().title()).contains("Produções");
    }
    @Quando("^clico em uma produção técnica$")
    public void clicoEmUmaProducaoTec() {
        waitUntilDisplayed(el(".resultado__titulo"));
        waitUntilDisplayed(el(".resultado__titulo").el("a"));
        waitUntilCliclableAndClick(el(".resultado__titulo").el("a"));
    }



    @Quando("^informo o nome da produção \"([^\"]*)\"$")
    public void informoOCriterioProd(String nome) throws Throwable {
        el("input#input-criterio").fill().with(nome);
    }
    @Entao("^aparece o resultado com o nome da produção \"([^\"]*)\"$")
    public void apareceOResultadoComOCriterio(String nome) throws Throwable {
        waitUntilDisplayed(el("#portfolio-pessoa-nome"));
        assertThat(el("#portfolio-pessoa-nome").text()).containsIgnoringCase(nome);
    }

    
    @Quando("^clico em produção bibliográfica$")
    public void clicoEmProducaoBibliografica() {
        el("#tab-producao_bibliografica___BV_tab_button__").click();
    }
    
    @E("^seleciono uma produção bibliográfica$")
    public void selecionoUmAProducaoBibliografica() {
        waitUntilDisplayed(el("#tab-producao_bibliografica"));
        el("#tab-producao_bibliografica").el("a").click();
    }
    
    @Quando("^clico em produção artística e cultural$")
    public void clicoEmProducaoArtisticaECultural() {
        el("#tab-producao_artistica_cultural___BV_tab_button__").click();
    }
    
    @E("^seleciono uma produção artística e cultural$")
    public void selecionoUmaProducaoArtisticaECultural() {
        waitUntilDisplayed(el("#tab-producao_artistica_cultural"));
        el("#tab-producao_artistica_cultural").el("a").click();
    }


    @Quando("^clico em projetos$")
    public void clicoEmProjetos() {
        el("#tab-projeto___BV_tab_button__").click();
    }
    
    @E("^seleciono um projeto$")
    public void selecionoUmProjeto() {
        waitUntilDisplayed(el("#tab-projeto"));
        el("#tab-projeto").el("a").click();
    }
    
 
    @Entao("^aparece o projeto selecionado e os seus dados$")
    public void apareceOProjetoSelecionadoEOsSeusDados() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".projeto__titulo-secao")).displayed();
    	assertThat(el(".projeto__titulo-secao")).isNotNull();
    }
    
    
    
    @E("^solicito buscar produções$")
    public void solicitoBuscarProd() {
        el("#input-criterio").submit();
    }




}
