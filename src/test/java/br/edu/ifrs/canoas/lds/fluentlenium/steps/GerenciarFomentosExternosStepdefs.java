package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class GerenciarFomentosExternosStepdefs extends BaseTest{
	
private static final String GERENCIAR_FOMENTOS = Constants.URL + "admin/ep/fomentos";
	
	@Dado("^estou na página fomento externo$")
	public void estouNaPaginaFomentoExterno(){
		 goTo(GERENCIAR_FOMENTOS);
	     assertThat(window().title()).contains("Administração");
	}
	
	@Quando("^clico em notificar um fomento$")
	public void clicoEmNotificarUmFomento(){
		waitUntilCliclableAndClick(el("#btn-notificar"));
	}
	
	@E("^informo os dados do novo fomento$")
	public void informoOsDadosDoNovoFomento() {
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("FomentoTeste");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Essa é uma parceria com a OrgTeste");
		
				
		waitUntilDisplayed(el("#palavrasChave"));
		el("#palavrasChave").fill().withText("fomento");
		el("#palavrasChave").keyboard().sendKeys(Keys.ENTER);
		
		el("#vs3__combobox").click();
		waitUntilCliclableAndClick(el("#vs3__option-12"));
        
        el("#dataLimite__value_").click();
        waitUntilDisplayed(el(".b-calendar-grid-body"));       
        el("#dataLimite__dialog_").keyboard().sendKeys(Keys.ENTER);
        
        el("#vs4__combobox").click();
        waitUntilCliclableAndClick(el("#vs4__option-6"));
        el("#vs5__combobox").click();
        waitUntilCliclableAndClick(el("#vs5__option-1"));
        waitUntilDisplayed(el("#valorTotalFinanciado"));
		el("#valorTotalFinanciado").fill().withText("1200");
		waitUntilDisplayed(el("#emailContato"));
		el("#emailContato").fill().withText("fomentoteste@gmail.com");
		waitUntilDisplayed(el("#urlFomento"));
		el("#urlFomento").fill().withText("https://www.fomentoteste.com.br");
		
	}
	
	
	
	@Entao("^aparece o novo fomento criado na lista de fomentos$")
	public void apareceONovoFomentoCriadoNaListaDeFomentos() {
		await().atMost(10, TimeUnit.SECONDS).until(el(".painel-interno")).present();
		assertThat(el(".painel-interno").text()).containsIgnoringCase("FomentoTeste");
	}
	
	
	
	@Quando("^clico em um fomento$")
	public void clicoEmUmFomento(){
		waitUntilDisplayed(el("#tabela-resultados"));
        el("#tabela-resultados").el("td").click();
	}
	
	
	
	@E("^altero os dados do fomento$")
	public void alteroOsDadosDoFomento(){
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("FomentoTeste2021");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Bem vindo ao novo fomento de 2021");
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que o fomento foi alterado$")
	public void oSistemaApresentaUmaMensagemInformandoQueOFomentoFoiAlterado(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	@Entao("^o sistema apresenta uma mensagem informando que o fomento foi excluido$")
	public void oSistemaApresentaUmaMensagemInformandoQueOFomentoFoiExcluido(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
}
