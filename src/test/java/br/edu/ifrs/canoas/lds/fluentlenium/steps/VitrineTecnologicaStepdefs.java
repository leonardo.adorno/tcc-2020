package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

import java.util.concurrent.TimeUnit;

public class VitrineTecnologicaStepdefs extends BaseTest {

    private static final String VITRINE_TECNOLOGICA = Constants.URL + "vitrine";

    @Dado("^estou na página da vitrine tecnológica$")
    public void estouNaPaginaDaVitrineTecnologica() {
        goTo(VITRINE_TECNOLOGICA);
        assertThat(window().title()).contains("Vitrine Tecnológica");
    }
    @Quando("^clico em um produto ou serviço$")
    public void clicoEmUmProdutoOuServico() {
        waitUntilCliclableAndClick(el(".card-body").el("a"));
    }
    @Entao("^aparece o produto ou serviço e um texto indicando o processo no INPI$")
    public void apareceOProdutoOuServicoEUmTextoIndicandoOProcessoNoINPI() {
        assertThat(el(".tecnologia__info")).isNotNull();
    }




    @Quando("^clico nos filtros$")
    public void clicoNosFiltros() {
        waitUntilCliclableAndClick(el(".esconde-filtros"));
    }
    @E("^informo o nome do produto ou serviço \"([^\"]*)\"$")
    public void informoONomeDoProdutoOuServico(String nome) throws Throwable {
        el("input#criteria_1").fill().withText(nome);
    }
    @E("^solicito buscar produtos ou serviços$")
    public void solicitoBuscarProdutosOuServicos() {
        el("#criteria_1").submit();
    }
    @Entao("^aparece o resultado com o nome do produto ou serviço \"([^\"]*)\"$")
    public void apareceOResultadoComONomeDoProdutoOuServico(String nome) throws Throwable {
        waitUntilDisplayed(el(".tecnologia__titulo"));
        assertThat(el(".tecnologia__titulo").text()).containsIgnoringCase(nome);
    }



    @E("^informo a categoria engenharias$")
    public void informoACategoriaEngenharias() {
    	waitUntilCliclableAndClick(el("#categorias"));
    	waitUntilCliclableAndClick(el("#vs1__option-1"));
    }
    @Entao("^aparece o resultado com a categoria engenharias$")
    public void apareceOResultadoComACategoriaExatasEDaTerra() {
        waitUntilDisplayed(el(".lista"));
        assertThat(el(".lista", containingText("Engenharias"))).isNotNull();
    }



    @E("^informo a propriedade intelectual Software$")
    public void informoAPropriedadeIntelectualSoftware() {
        el("#tpProprIntelectual").click();
        waitUntilCliclableAndClick(el("#vs2__option-2"));
    }
    @Entao("^aparece o resultado com um produto ou serviço da propriedade intelectual Software$")
    public void apareceOResultadoComUmProdutoOuServicoDaPropriedadeIntelectualSoftware() {
        waitUntilDisplayed(el(".tecnologia__titulo"));
        assertThat(el(".tecnologia__titulo", containingText("Software"))).isNotNull();
    }




    @E("^clico no nível de maturidade do projeto$")
    public void clicoNoNivelDeMaturidadeDoProjeto() {
        waitUntilCliclableAndClick(el(".cls-1"));
    }
    @Entao("^o sistema apresenta o nível de maturidade tecnológica do projeto$")
    public void oSistemaApresentaONivelDeMaturidadeTecnologicaDoProjeto() {
        assertThat(el("#modal-trl___BV_modal_title_", containingText("Technology Readiness Level"))).isNotNull();
    }


}