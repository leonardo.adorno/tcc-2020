package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class EPVideotecaStepdefs extends BaseTest {


    private static final String VIDEOTECA = Constants.URL + "ep/videoteca";

    @Dado("^estou na página do escritório de projetos videoteca$")
    public void estouNaPaginaDoEscritorioDeProjetosVideoteca() {
        goTo(VIDEOTECA);
        assertThat(window().title()).contains("Videoteca");
    }
    
    @Quando("^clico em empreendedorismo$")
    public void clicoEmEmpre() {
    	waitUntilCliclableAndClick(el("#tab-1___BV_tab_button__"));
    }
    
    @E("^clico em um vídeo$")
    public void clicoEmUmVideo() {
        waitUntilCliclableAndClick(el(".btn-outline-primary", withText("SAIBA MAIS")));
    }
    @Entao("^aparece o vídeo e o seu autor$")
    public void apareceOVideoEOSeuAutor() {
        assertThat(el("p", containingText("Autor:"))).isNotNull();
    }




    @Quando("^informo o nome do vídeo \"([^\"]*)\"$")
    public void informoONomeDoVideo(String nome) throws Throwable {
        el("#criteria_1").fill().withText(nome);
    }
    @E("^solicito buscar vídeos$")
    public void solicitoBuscarFomentos() {
        el("#criteria_1").submit();
    }
    @Entao("^aparece o resultado com o nome do vídeo \"([^\"]*)\"$")
    public void apareceOResultadoComONomeDoVideo(String nome) throws Throwable {
        assertThat(el("#modal-video0-1___BV_modal_title_").text()).isNotNull();
    }
    
    
    
    @Quando("^clico em Habitats de Inovação$")
    public void clicoEmHabitatsDeInovacao() {
        waitUntilCliclableAndClick(el("#tab-2___BV_tab_button__"));
    }
    
    
    @Quando("^clico em Inovação$")
    public void clicoEmInovacao() {
        waitUntilCliclableAndClick(el("#tab-3___BV_tab_button__"));
    }
    
    
    @Quando("^clico em Legislação$")
    public void clicoEmLegislacao() {
        waitUntilCliclableAndClick(el("#tab-4___BV_tab_button__"));
    }
    
    
    @Quando("^clico em Novas Tecnologias$")
    public void clicoEmNovasTecnologias() {
        waitUntilCliclableAndClick(el("#tab-6___BV_tab_button__"));
    }

    
    @Quando("^clico em Propriedade Intelectual$")
    public void clicoEmPropriedadeIntelectual() {
        waitUntilCliclableAndClick(el("#tab-5___BV_tab_button__"));
    }

  
}
