package br.edu.ifrs.canoas.lds.fluentlenium.config;

import io.cucumber.core.gherkin.Feature;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assume;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class FluentHooks extends BaseTest {

    public FluentHooks() {
        WebDriverManager.chromedriver().setup();
    }

    @Before("@skip_scenario")
    public void skip_scenario(Scenario scenario){
        Assume.assumeTrue(false);
    }

    @Before("@admin")
    public void admin(Scenario scenario){

        before(scenario);
        goTo(Constants.URL);
        assertThat(window().title()).contains("Integra IFRS - Portal da Inovação");
        await().atMost(5, TimeUnit.SECONDS).until(el(".autenticacao").el("a", withText("Entrar"))).clickable();
        el(".autenticacao").el("a", withText("Entrar")).click().switchTo();
        String originalWindow = getDriver().getWindowHandle();
        for (String windowHandle : getDriver().getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                getDriver().switchTo().window(windowHandle);
                break;
            }
        }
        el("#identifierId").fill().with("integraifrs@gmail.com");
        el("button", withText("Próxima")).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), 100);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password']")));
        waitAndFill(find(By.xpath("//input[@type='password']")).first(), "ifrs2021");
        waitUntilCliclableAndClick(el("button", withText("Próxima")));
        getDriver().switchTo().window(originalWindow);
        waitUntilDisplayed(el("p.autenticacao__usuario"));
        assertThat(el("p.autenticacao__usuario").text()).isEqualTo("INTEGRA IFRS");
    }

    @Before
    public void beforeScenario(Scenario scenario) {
        before(scenario);
    }

    @After
    public void afterScenario(Scenario scenario) {
        after(scenario);
    }
}