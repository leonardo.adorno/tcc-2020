package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.service.DriverService;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.withText;
import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfWindowsToBe;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;

public class AutenticacaoStepdefs extends BaseTest {

    String originalWindow = null;

    @Dado("estou na página inicial do Integra")
    public void estouNaPáginaInicialDoIntegra() {
        goTo(Constants.URL);
        assertThat(window().title()).contains("Integra IFRS - Portal da Inovação");
    }

    @Quando("clico em entrar no sistema")
    public void clicoEmEntrarNoSistema() {
        await().atMost(5, TimeUnit.SECONDS).until(el(".autenticacao").el("a", withText("Entrar"))).clickable();
        el(".autenticacao").el("a", withText("Entrar")).click().switchTo();
    }

    @E("informo credenciais válidas")
    public void informoCredenciaisValidas(){
        originalWindow = getDriver().getWindowHandle();
        for (String windowHandle : getDriver().getWindowHandles()) {
            if(!originalWindow.contentEquals(windowHandle)) {
                getDriver().switchTo().window(windowHandle);
                break;
            }
        }
        el("#identifierId").fill().with("integraifrs@gmail.com");
        el("button", withText("Próxima")).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), 100);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password']")));
        waitAndFill(find(By.xpath("//input[@type='password']")).first(), "ifrs2021");
        waitUntilCliclableAndClick(el("button", withText("Próxima")));
    }

    @Entao("É apresentada a tela inicial com o usuário autenticado")
    public void eApresentadaATelaInicialExclusivaDeServidores() {
        for (String windowHandle : getDriver().getWindowHandles()) {
            if(originalWindow.contentEquals(windowHandle)) {
                getDriver().switchTo().window(windowHandle);
                break;
            }
        }


        waitUntilDisplayed(el("p.autenticacao__usuario"));
        assertThat(el("p.autenticacao__usuario").text()).isEqualTo("INTEGRA IFRS");
    }
}
