package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

import java.util.concurrent.TimeUnit;

public class OrganizacoesParceirasStepdefs extends BaseTest {

    private static final String ORGANIZACOES_PARCEIRAS = Constants.URL + "parcerias";

    @Dado("^estou na página da organizações parceiras$")
    public void estouNaPaginaDaOrganizacoesParceiras() {
        goTo(ORGANIZACOES_PARCEIRAS);
        assertThat(window().title()).contains("Organizações Parceiras");
    }
    @Quando("^clico em uma organização parceira$")
    public void clicoEmUmaOrganizacaoParceira() {
        waitUntilCliclableAndClick(el("div.card"));
    }
    @Entao("^aparece a organização parceira selecionada vinculada com o extrato de publicação no DOU$")
    public void apareceAOrganizacaoParceiraSelecionadaVinculadaComOExtratoDePublicacaoNoDOU() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".mt-0")).displayed();
        assertThat(el(".mt-0")).isNotNull();
    }



    @E("^informo o nome da organização parceira \"([^\"]*)\"$")
    public void informoONomeDaOrganizacaoParceira(String nome) throws Throwable {
        el("#criteria_1").fill().with(nome);
    }
    @E("^solicito buscar organizações parceiras$")
    public void solicitoBuscarOrganizacoesParceiras() {
        el("#criteria_1").submit();
    }
    @Entao("^aparece o resultado com o nome da organização parceira \"([^\"]*)\"$")
    public void apareceOResultadoComONomeDaOrganizacaoParceira(String nome) throws Throwable {
        waitUntilCliclableAndClick(el("div.card"));
    	await().atMost(5, TimeUnit.SECONDS).until(el("mt-0")).present();
    	assertThat(el("mt-0").text()).isNotNull();
    }


}