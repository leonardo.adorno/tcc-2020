package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class ComumStepdefs extends BaseTest {

    @Quando("^solicito buscar")
    public void solicitoBuscar() throws InterruptedException {
        await().atMost(2, TimeUnit.SECONDS).until(el("#botao-buscar")).clickable();
        el("#botao-buscar").waitAndClick();
        await().atMost(2, TimeUnit.SECONDS).until(el("#botao-buscar")).not().clickable();
        await().until(() -> $(".item-resultado").first().displayed());
    }

    @Quando("^clico em um resultado")
    public void clicoEmUmResultado() {
        await().atMost(2, TimeUnit.SECONDS).until(el("#botao-buscar")).not().clickable();
        await().until(() -> $(".item-resultado").first().displayed());
        waitUntilCliclableAndClick(el(".item-resultado"));
    }
}
