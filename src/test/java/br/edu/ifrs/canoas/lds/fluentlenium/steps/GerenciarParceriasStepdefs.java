package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class GerenciarParceriasStepdefs extends BaseTest{
	
	private static final String GERENCIAR_PARCERIAS = Constants.URL + "parceria";
	
	@Dado("^estou na página parcerias$")
	public void estouNaPaginaParcerias(){
		 goTo(GERENCIAR_PARCERIAS);
	     assertThat(window().title()).contains("Parcerias");
	}
	
	@Quando("^clico em nova parceria$")
	public void clicoEmNovaParceria(){
		waitUntilCliclableAndClick(el("#botao-novo"));
	}
	
	@E("^informo os dados da nova parceria$")
	public void informoOsDadosDaNovaParceria() {
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("ParceriaOrgTeste");
		
		
		waitUntilDisplayed(el("#selecao-organizacao"));
		el("#selecao-organizacao").click();
		el("#selecao-organizacao").fill().withText("OT2");
		waitUntilDisplayed(el("#vs4__listbox"));
		waitUntilCliclableAndClick(el("#vs4__option-2"));
		
		//waitUntilDisplayed(el("#vs6__combobox"));
		//waitUntilCliclableAndClick(el("#vs6__option-2"));
		
        waitUntilDisplayed(el("#urlRepositorio"));
		el("#urlRepositorio").fill().withText("https://www.parorgteste.com.br");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Essa é uma parceria com a OrgTeste");   
	}
	
	
	
	@Entao("^aparece a nova parceria criada na lista de parcerias$")
	public void apareceANovaParceriaCriadaNaListaDeParcerias() {
		await().atMost(10, TimeUnit.SECONDS).until(el(".painel-interno")).present();
		assertThat(el(".painel-interno").text()).containsIgnoringCase("ParceriaOrgTeste");
	}
	
	
	
	@Quando("^clico em uma parceria$")
	public void clicoEmUmaParceria(){
		waitUntilCliclableAndClick(el("#criteria_1"));
		el("#criteria_1").fill().withText("teste");
		waitUntilCliclableAndClick(el("#botao-buscar"));
		waitUntilDisplayed(el(".resultado__titulo"));
        el(".resultado__titulo").el("a").click();
		await().atMost(5, TimeUnit.SECONDS).until(el(".painel-interno")).displayed();
	}
	
	
	
	@E("^altero os dados da parceria$")
	public void alteroOsDadosDaParceria(){
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("ParceriaOrgTesteNova");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Bem vindo a nova parceria");
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que a parceria foi alterada$")
	public void oSistemaApresentaUmaMensagemInformandoQueAParceriaFoiAlterada(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	@Entao("^o sistema apresenta uma mensagem informando que a parceria foi excluida$")
	public void oSistemaApresentaUmaMensagemInformandoQueAParceriaFoiExcluida(){
		assertThat(el(".toast-body")).isNotNull();
	}

}
