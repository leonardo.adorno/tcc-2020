package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class GerenciarLaboratoriosStepdefs extends BaseTest{
	
private static final String GERENCIAR_LABORATORIOS = Constants.URL + "admin/laboratorios";
	
	@Dado("^estou na página laboratórios$")
	public void estouNaPaginaLaboratorio(){
		 goTo(GERENCIAR_LABORATORIOS);
	     assertThat(window().title()).contains("Administração");
	}
	
	@Quando("^clico em novo laboratório$")
	public void clicoEmNovoLaboratório(){
		 waitUntilCliclableAndClick(el("button.btn-outline-primary", withText("NOVO LABORATÓRIO")));
	}
	
	@E("^informo os dados do novo laboratório$")
	public void informoOsDadosDoNovoLaboratorio() {
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("NovoLaboratório");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Essa é o NovoLaboratório, o mais novo laboratório do ifrs");
		waitUntilDisplayed(el("#vs4__combobox"));
		el("#vs4__combobox").click();
		waitUntilCliclableAndClick(el("#vs4__option-3"));
		
	    
	    waitUntilDisplayed(el("#selecao-servidores"));
		el("#selecao-servidores").click();
		el("#selecao-servidores").fill().withText("Leo");
		waitUntilDisplayed(el("#vs5__listbox"));
		waitUntilCliclableAndClick(el("#vs5__option-3"));
	    
		
		waitUntilDisplayed(el("#urlInstitucional"));
		el("#urlInstitucional").fill().withText("https://novolaboratorio.com.br");
		waitUntilDisplayed(el("#email"));
		el("#email").fill().withText("novolaboratorio@gmail.com");
		waitUntilDisplayed(el("#urlRegulamento"));
		el("#urlRegulamento").fill().withText("https://regnovolaboratorio.com.br");
		waitUntilDisplayed(el("#urlAgenda"));
		el("#urlAgenda").fill().withText("https://agendanovolaboratorio.com.br");
		
		waitUntilDisplayed(el("#servicos"));
		el("#servicos").fill().withText("Ele realiza diversos serviços");
		waitUntilDisplayed(el("#equipamentos"));
		el("#equipamentos").fill().withText("Ele possui diversos equipamentos");
		waitUntilDisplayed(el("#software"));
		el("#software").fill().withText("Ele possui diversos softwares disponíveis");
				
	}
	
	
	
	@Entao("^aparece o novo laboratório criado na lista de laboratórios$")
	public void apareceONovoLaboratorioCriadoNaListaDeLaboratorios() {
		await().atMost(5, TimeUnit.SECONDS).until(el(".painel-interno")).present();
		assertThat(el(".painel-interno").text()).containsIgnoringCase("NovoLaboratório");
	}
	
	
	
	@Quando("^clico em um laboratório$")
	public void clicoEmUmLaboratório(){
		waitUntilCliclableAndClick(el("#criteria_1"));
		el("#criteria_1").fill().withText("NovoLaboratório");
		waitUntilCliclableAndClick(el("#botao-buscar"));
		waitUntilDisplayed(el("#tabela-resultados"));
        el("#tabela-resultados").el("td").click();
	}
	
	
	
	@E("^altero os dados do laboratório$")
	public void alteroOsDadosDaTecnologia(){
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("Agroindústria1");
		waitUntilDisplayed(el("#software"));
		el("#software").fill().withText("AutoCad. Ele possui diversos softwares disponíveis");
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que o laboratório foi alterado$")
	public void oSistemaApresentaUmaMensagemInformandoQueOLaboratorioFoiAlterado(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	@Entao("^o sistema apresenta uma mensagem informando que o laboratório foi excluido$")
	public void oSistemaApresentaUmaMensagemInformandoQueOLaboratorioFoiExcluido(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
}
