package br.edu.ifrs.canoas.lds.fluentlenium.config;

import io.cucumber.java8.Pt;
import org.fluentlenium.adapter.cucumber.FluentCucumberTest;
import org.fluentlenium.configuration.ConfigurationProperties;
import org.fluentlenium.configuration.FluentConfiguration;
import org.fluentlenium.core.domain.FluentWebElement;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;


@FluentConfiguration(webDriver = "chrome"
        , driverLifecycle = ConfigurationProperties.DriverLifecycle.JVM)
public class BaseTest extends FluentCucumberTest  implements Pt {

    @Override
    public Capabilities getCapabilities() {
       // Headless Chrome
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless");
//        options.addArguments("--window-size=1920,1200");
//        options.addArguments("--start-maximized");
//        options.addArguments("--disable-gpu");
//        options.addArguments("--no-sandbox");
//        options.addArguments("--log-level=2");
//        options.addArguments("--lang=pt-br");
//        options.addArguments("--disable-dev-shm-usage");
//        options.addArguments("--disable-web-security");
//        options.addArguments("--disable-extensions");
//        options.addArguments("--allow-insecure-localhost");
//        options.addArguments("--ignore-urlfetcher-cert-requests");
//        options.addArguments("--ignore-certificate-errors");
//        options.addArguments("--proxy-bypass-list=*");
//        options.addArguments("--proxy-server='direct://'");
//        options.addArguments("--user-agent=\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36\"");
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//        capabilities.setCapability("chromeOptions", options);
//        return capabilities;
        return new ChromeOptions();
    }


    public void waitUntilCliclableAndClick(FluentWebElement element) {
        await().atMost(5, TimeUnit.SECONDS).until(element).clickable();
        element.click();
    }

    public void waitAndFill(FluentWebElement element, String data) {
        await().atMost(5, TimeUnit.SECONDS).until(element).displayed();
        element.fill().with(data);
    }

    public void waitUntilDisplayed(FluentWebElement element) {
        await().atMost(5, TimeUnit.SECONDS).until(element).displayed();
    }

}
