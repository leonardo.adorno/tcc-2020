package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class EPFomentosExternosStepdefs extends BaseTest {

    private static final String FOMENTOS_EXTERNOS = Constants.URL + "ep/fomentos";

    @Dado("^estou na página do escritório de projetos fomentos externos$")
    public void estouNaPaginaDoEscritorioDeProjetosFomentosExternos() {
        goTo(FOMENTOS_EXTERNOS);
        assertThat(window().title()).contains("Fomentos Externos");
    }
    @Quando("^clico em um fomento externo vigente$")
    public void clicoEmUmFomentoExternoVigente() {
        waitUntilCliclableAndClick(el(".item__titulo").el("a"));
    }
    @Entao("^aparece o fomento extereno vigente com a última atualização$")
    public void apareceOFomentoExterenoVigenteComAUltimaAtualizacao() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".fomento__info")).displayed();
        assertThat(el(".fomento__info")).isNotNull();
    }




    @Quando("^informo o nome do fomento \"([^\"]*)\"$")
    public void informoONomeDoFomento(String nome) throws Throwable {
        el("input#criteria_1").fill().withText(nome);
    }
    @E("^solicito buscar fomentos$")
    public void solicitoBuscarFomentos() {
        el("#criteria_1").submit();
    }
    @Entao("^aparece o resultado com o nome do fomento \"([^\"]*)\"$")
    public void apareceOResultadoComONomeDoFomento(String nome) throws Throwable {
        await().atMost(5, TimeUnit.SECONDS).until(el(".fomento__titulo")).present();
        assertThat(el(".fomento__titulo").text()).containsIgnoringCase(nome);
    }



    @E("^informo a entidade financiadora CAPES$")
    public void informoAEntidadeFinanciadoraCAPES() {
        el("#financ").click();
        waitUntilCliclableAndClick(el("#vs1__option-6"));
    }
    @Entao("^aparece o resultado com a entidade financiadora CAPES$")
    public void apareceOResultadoComAEntidadeFinanciadoraCAPES() {
        waitUntilDisplayed(el(".fomento__titulo"));
        assertThat(el("small", containingText("CAPES"))).isNotNull();
    }



    @Quando("^informo o tipo Concursos e Prêmios$")
    public void informoOTipoConcursosEPrêmios() {
        el("#fomentos").click();
        waitUntilCliclableAndClick(el("#vs2__option-3"));
    }
    @Entao("^aparece o resultado com o tipo Concursos e Prêmios$")
    public void apareceOResultadoComOTipoConcursosEPremios() {
        waitUntilDisplayed(el(".fomento__titulo"));
        assertThat(el("li", containingText("Concursos e Prêmios"))).isNotNull();
    }



    @Quando("^clico nos fomentos externos encerrados$")
    public void clicoNosFomentosExternosEncerrados() {
        el("#tab-fomento-encerrados___BV_tab_button__").click();
    }
    
    @E("^clico em um fomento externo encerrado$")
    public void clicoEmUmFomentoExternoEncerrado() {
    	waitUntilDisplayed(el("#tab-fomento-encerrados"));
    	waitUntilCliclableAndClick(el(".item__titulo").el("a"));
    	
    }

    @Entao("^aparece o fomento extereno encerrado com a última atualização$")
    public void apareceOFomentoExterenoEncerradoComAUltimaAtualização() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".fomento__info")).displayed();
    	assertThat(el(".fomento__info")).isNotNull();
    }


}
