package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class GerenciarFluxosENormasStepdefs extends BaseTest{
	
private static final String GERENCIAR_FLUXOS = Constants.URL + "admin/ep/fluxos";
	
	@Dado("^estou na página fluxos e normas$")
	public void estouNaPaginaFluxosENormas(){
		 goTo(GERENCIAR_FLUXOS);
	     assertThat(window().title()).contains("Administração");
	}
	
	@Quando("^clico em novo fluxo ou norma$")
	public void clicoEmNovoFluxoOuNorma(){
		waitUntilCliclableAndClick(el("#btn-criar"));
	}
	
	@E("^informo os dados do novo fluxo ou norma$")
	public void informoOsDadosDoNovoFluxoOuNorma() {
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("FluxoTeste");
		waitUntilDisplayed(el("#resumo"));
		el("#resumo").fill().withText("Esse é o fluxoTeste");
		waitUntilDisplayed(el("#urlSaibaMais"));
		el("#urlSaibaMais").fill().withText("https://www.fluxoteste.com.br");
		waitUntilDisplayed(el("#urlDocumentos"));
		el("#urlDocumentos").fill().withText("https://www.docsfluxoteste.com.br");
		
		//PROBLEMA - ADICIONAR UM ID NA CAIXA DE TEXTO DESCRIÇÃO GERAL
		//waitUntilDisplayed(el("#tiny-vue_99519480321615593199529_ifr"));
		//el("#tiny-vue_99519480321615593199529_ifr").fill().withText("Essa é a descrição do no fluxoTeste");
		
		//PROBLEMA - ADICIONAR UM ID NA CAIXA DE TEXTO DESCRIÇÃO PARA PROPONENTE
		//waitUntilDisplayed(el("#tiny-vue_96815078531615593199550_ifr"));
		//el("#tiny-vue_96815078531615593199550_ifr").fill().withText("Essa irá ser outra descrição sobre o fluxoTeste");
		
	}
	
	
	
	@Entao("^aparece o novo fluxo ou norma criado na lista de fluxos e normas$")
	public void apareceONovoFluxoOuNormaCriadoNaListaDeFluxosENormas() {
		await().atMost(5, TimeUnit.SECONDS).until(el(".painel-interno")).present();
		assertThat(el(".painel-interno").text()).containsIgnoringCase("FluxoTeste");
	}
	
	
	
	@Quando("^clico em um fluxoo ou normaa$")
	public void clicoEmUmFluxoOuNorma(){
		waitUntilCliclableAndClick(el("#criteria_1"));
		el("#criteria_1").fill().withText("teste");
		waitUntilCliclableAndClick(el("#botao-buscar"));
		waitUntilDisplayed(el("#tabela-resultados"));
        el("#tabela-resultados").el("td").click();
	}
	
	
	
	@E("^altero os dados do fluxo ou norma$")
	public void alteroOsDadosDoFluxoOuNorma(){
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("FluxoTeste20");
		waitUntilDisplayed(el("#resumo"));
		el("#resumo").fill().withText("Bem vindo ao novo fluxo de 2021");
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que o fluxo ou norma foi alterado$")
	public void oSistemaApresentaUmaMensagemInformandoQueOFluxoOuNormaFoiAlterado(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	@Entao("^o sistema apresenta uma mensagem informando que o fluxo ou norma foi excluido$")
	public void oSistemaApresentaUmaMensagemInformandoQueOFluxoOuNormaFoiExcluido(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
}
