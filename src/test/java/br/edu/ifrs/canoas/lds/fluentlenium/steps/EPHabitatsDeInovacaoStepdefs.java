package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class EPHabitatsDeInovacaoStepdefs extends BaseTest {


    private static final String HABITATS_INOVACAO = Constants.URL + "ep/habitats";

    @Dado("^estou na página do escritório de projetos habitats de inovação$")
    public void estouNaPaginaDoEscritorioDeProjetosHabitatsDeInovacao() {
        goTo(HABITATS_INOVACAO);
        assertThat(window().title()).contains("Habitats de Inovação");
    }
    @Quando("^clico em um habitat de inovação$")
    public void clicoEmUmHabitatDeInovacao() {
    	waitUntilCliclableAndClick(el(".ep-habitats").el("a"));
    }
    
}