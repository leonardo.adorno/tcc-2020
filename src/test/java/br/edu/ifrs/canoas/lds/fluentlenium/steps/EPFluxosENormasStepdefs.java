package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class EPFluxosENormasStepdefs extends BaseTest {

    private static final String FLUXO_NORMAS = Constants.URL + "ep/fluxos";

    @Dado("^estou na página do escritório de projetos fluxos e normas$")
    public void estouNaPaginaDoEPFluxosENormas() {
        goTo(FLUXO_NORMAS);
        assertThat(window().title()).contains("Fluxos e Normas");
    }
    @Quando("^clico em um fluxo ou norma$")
    public void clicoEmUmFLuxoOuNorma() {
        waitUntilCliclableAndClick(el(".item__titulo").el("a"));
    }
    @Entao("^aparece o fluxo ou norma com a última atualização$")
    public void apareceOFluxoOuNormaComAUltimaAtualização() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".fluxo__info")).displayed();
        assertThat(el(".fluxo__info")).isNotNull();
    }




    @Quando("^informo o nome do fluxo ou norma \"([^\"]*)\"$")
    public void informoONomeDoFluxoOuNorma(String nome) throws Throwable {
        el("input#criteria_1").fill().withText(nome);
    }
    @E("^solicito buscar fluxos ou normas$")
    public void solicitoBuscarFluxosOuNormas() {
        el("#criteria_1").submit();
    }
    @Entao("^aparece o resultado com o nome do fluxo ou norma \"([^\"]*)\"$")
    public void apareceOResultadoComONomeDoFluxoOuNorma(String nome) throws Throwable {
        await().atMost(5, TimeUnit.SECONDS).until(el(".fluxo__titulo")).present();
        assertThat(el(".fluxo__titulo").text()).containsIgnoringCase(nome);
    }

    
    
    @E("^clico na opção completo$")
    public void clicoNaOpcaoCompleto() {
        waitUntilDisplayed(el("#tab-fluxo-completo___BV_tab_button__"));
        el("#tab-fluxo-completo___BV_tab_button__").click();
    }
    @Entao("^aparece o fluxo completo do fluxo ou norma selecionado$")
    public void apareceOFluxoCompletoDoFluxoOuNormaSelecionado() {
    	await().atMost(5, TimeUnit.SECONDS).until(el("#tab-fluxo-completo")).displayed();
        assertThat(el("#tab-fluxo-completo")).isNotNull();
    }


}
