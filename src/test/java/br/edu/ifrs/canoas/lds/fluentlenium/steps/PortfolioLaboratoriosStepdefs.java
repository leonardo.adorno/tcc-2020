package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

import java.util.concurrent.TimeUnit;

public class PortfolioLaboratoriosStepdefs extends BaseTest {

    private static final String PORTFOLIO_LABORATORIOS = Constants.URL + "portfolio/laboratorios";


    @Dado("^estou na página do portfólio laboratórios$")
    public void estouNaPaginaDoPortfolioLaboratorios() {
        goTo(PORTFOLIO_LABORATORIOS);
        assertThat(window().title()).contains("Laboratórios");
    }

    @Entao("^aparece o laboratório e um texto indicando o responsável por ele$")
    public void apareceOLaboratorioEUmTextoIndicandoOResponsavelPorEle() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".infra__info")).displayed();
        assertThat(el(".infra__info")).isNotNull();
    }



    @Quando("^informo o nome do laboratório \"([^\"]*)\"$")
    public void informoOCriterioLab(String nome) throws Throwable {
        el("#input-criterio").fill().withText(nome);
    }
    @Entao("^aparece o resultado com o nome do laboratório \"([^\"]*)\"$")
    public void apareceOResultadoComONomeDoLaboratorio(String nome) throws Throwable{
    	await().atMost(5, TimeUnit.SECONDS).until(el(".infra__titulo")).present();
        assertThat(el(".infra__titulo").text()).containsIgnoringCase(nome);
    }

    @Entao("^aparece o resultado com o laboratório do campus Canoas$")
    public void apareceOResultadoComOLaboratorioDoCampusCanoas() throws Throwable{
        waitUntilDisplayed(el(".infra__titulo"));
        assertThat(el(".infra__titulo").text()).containsIgnoringCase("Canoas");
    }

    @Quando("^clico em habitats de inovação$")
    public void clicoEmHabitatsDeInovacao() {
        el("#tab-habitat___BV_tab_button__").click();
    }

    @Quando("^clico em um habitat$")
    public void clicoEmUmResultado() {
        el("#tab-habitat").el(".item-resultado").waitAndClick();
    }
    
    @E("^solicito buscar laboratorios$")
    public void solicitoBuscarLab() {
        el("#input-criterio").submit();
    }
    
    @Entao("^aparece o habitat de inovação e um texto indicando os equipamentos presentes nele$")
    public void apareceOHabitatDeInovacao() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".infra__info")).displayed();
        assertThat(el(".infra__info")).isNotNull();
    }

}

