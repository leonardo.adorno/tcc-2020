package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.Keys;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class GerenciarVitrineTecnologicaStepdefs extends BaseTest{
	
private static final String GERENCIAR_VITRINE = Constants.URL + "admin/vitrine";
	
	@Dado("^estou na página vitrine tecnológica$")
	public void estouNaPaginaVitrineTecnologica(){
		 goTo(GERENCIAR_VITRINE);
	     assertThat(window().title()).contains("Administração");
	}
	
	@Quando("^clico em nova tecnologia$")
	public void clicoEmNovaTecnologia(){
		waitUntilCliclableAndClick(el("#btn-novo"));
	}
	
	@E("^informo os dados da nova tecnologia$")
	public void informoOsDadosDaNovaTecnologia() {
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("NovaTec");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Essa é a NovaTec, a mais nova tecnologia do ano");


		waitUntilDisplayed(el("#palavrasChave"));
		el("#palavrasChave").fill().withText("tecnologia");
		el("#palavrasChave").keyboard().sendKeys(Keys.ENTER);


		waitUntilDisplayed(el("#vs3__combobox"));
		el("#vs3__combobox").click();
		waitUntilCliclableAndClick(el("#vs3__option-3"));
	    waitUntilDisplayed(el("#problemaResolvido"));
		el("#problemaResolvido").fill().withText("Ela resolve diversos problemas da atualidade");
		waitUntilDisplayed(el("#aplicacoes"));
		el("#aplicacoes").fill().withText("Ela possue diversos diferencias");
		waitUntilDisplayed(el("#vantagens"));
		el("#vantagens").fill().withText("Ela possue diversas vantagens");
		waitUntilDisplayed(el("#email"));
		el("#email").fill().withText("novatec@gmail.com");

		
		waitUntilDisplayed(el("#selecao-servidores"));
		el("#selecao-servidores").click();
		el("#selecao-servidores").fill().withText("Leo");
		waitUntilDisplayed(el("#vs4__listbox"));
		waitUntilCliclableAndClick(el("#vs4__option-3"));



		el("#transferencia-propriedade___BV_tab_button__").click();
		waitUntilDisplayed(el("#selecao-tp-transferencia"));
		el("#selecao-tp-transferencia").click();
		waitUntilCliclableAndClick( el("#vs6__option-1"));
	}


	
	@Entao("^aparece a nova tecnologia criada na lista de tecnologias$")
	public void apareceANovaParceriaCriadaNaListaDeParcerias() {
		await().atMost(5, TimeUnit.SECONDS).until(el(".painel-interno")).present();
		assertThat(el(".painel-interno").text()).containsIgnoringCase("NovaTec");
	}
	
	
	
	@Quando("^clico em uma tecnologia$")
	public void clicoEmUmaTecnologia(){
		waitUntilDisplayed(el("#tabela-resultados"));
        el("#tabela-resultados").el("td").click();
	}
	
	
	
	@E("^altero os dados da tecnologia$")
	public void alteroOsDadosDaTecnologia(){
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("NovaTec2020");
		waitUntilDisplayed(el("#descricao"));
		el("#descricao").fill().withText("Bem vindo a nova tecnologia");
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que a tecnologia foi alterada$")
	public void oSistemaApresentaUmaMensagemInformandoQueATecnologiaFoiAlterada(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	@Entao("^o sistema apresenta uma mensagem informando que a tecnologia foi excluida$")
	public void oSistemaApresentaUmaMensagemInformandoQueATecnologiaFoiExcluida(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
}
