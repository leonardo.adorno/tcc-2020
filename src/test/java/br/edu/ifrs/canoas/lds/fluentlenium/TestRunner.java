package br.edu.ifrs.canoas.lds.fluentlenium;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:cucumber",
        plugin = {"pretty", "html:target/cucumber.html", "json:target/cucumber.json"})
public class TestRunner {
}
