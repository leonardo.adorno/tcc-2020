package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

import java.util.concurrent.TimeUnit;

public class PrestacaoServicoStepdefs extends BaseTest {

    private static final String PRESTACAO_SERVICOS = Constants.URL + "servicos";

    @Dado("^estou na página da prestação de serviços$")
    public void estouNaPaginaDaPrestacaoDeServicos() {
        goTo(PRESTACAO_SERVICOS);
        assertThat(window().title()).contains("Prestação de Serviços");
    }
    @Quando("^clico em uma prestação de serviço de uma pessoa$")
    public void clicoEmUmaPrestacaoDeServicoDeUmaPessoa() {
        waitUntilCliclableAndClick(el(".resultado__titulo").el("a"));
    }





    @Quando("^informo o critério \"([^\"]*)\"$")
    public void informoOCriterio(String nome) throws Throwable {
        el("input#input-criterio").fill().with(nome);
    }
    @E("^solicito buscar prestação de serviços$")
    public void solicitoBuscarPres() {
        el("#input-criterio").submit();
    }
    @Entao("^aparece o resultado com a prestação de serviço \"([^\"]*)\"$")
    public void apareceOResultadoComAPrestacaoDeServico(String nome) throws Throwable {
        waitUntilDisplayed(el("#portfolio-pessoa-nome"));
        assertThat(el("#portfolio-pessoa-nome").text()).containsIgnoringCase(nome);
    }
    
    @E("^informo a reitoria$")
    public void informoAReitoria() {
    	waitUntilCliclableAndClick(el("#filtro-campus"));
    	waitUntilCliclableAndClick( el("#vs1__option-18"));
    }



    @Quando("^clico em laboratórios$")
    public void clicoEmLaboratorios() {
    	waitUntilCliclableAndClick(el("#tab-servico-laboratorios___BV_tab_button__"));
    }

    @E("^seleciono uma prestação de serviço de um laboratório$")
    public void selecionoUmaPrestacaoDeServicoDeUmLaboratorio() {
        waitUntilDisplayed(el("#tab-servico-laboratorios"));
        el(".infra-resultados").el("a").click();
    }
    @Entao("^aparece o laboratório e um texto indicando que é prestador de serviço$")
    public void apareceOLaboratorioEUmTextoIndicandoQueEPrestadorDeServico() {
        assertThat(el("p", containingText("PRESTADOR DE SERVIÇO"))).isNotNull();
    }



}

