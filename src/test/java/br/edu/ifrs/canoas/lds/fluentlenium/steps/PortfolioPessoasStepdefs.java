package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class PortfolioPessoasStepdefs extends BaseTest {

    private static final String PORTFOLIO_PESSOAS = Constants.URL + "portfolio/pessoas";

    @Dado("^estou na página do portfólio pessoas$")
    public void estouNaPaginaDoPortfolioPessoas() {
        goTo(PORTFOLIO_PESSOAS);
        assertThat(window().title()).contains("Pessoas");
    }
    @Entao("^aparece o currículo do servidor e um texto indicando a data de atualização do portfólio$")
    public void apareceOCurriculoDoServidorEUmTextoIndicandoADataDeAtualizacaoDoPortfolio() {
    	await().atMost(5, TimeUnit.SECONDS).until(el(".perfil__cargo")).displayed();
        assertThat(el(".perfil__cargo")).isNotNull();
    }




    @Quando("^informo o nome da pessoa \"([^\"]*)\"$")
    public void informoONome(String nome) throws Throwable {
        el("input#filtro-nome").fill().withText(nome);
    }
    @Entao("^aparece o resultado com o nome da pessoa \"([^\"]*)\"$")
    public void apareceOResultadoComONome(String nome) throws Throwable {
        await().atMost(5, TimeUnit.SECONDS).until(el("#portfolio-pessoa-nome")).present();
        assertThat(el("#portfolio-pessoa-nome").text()).containsIgnoringCase(nome);
    }



    @Quando("^informo o campus Canoas$")
    public void informoOCampusCanoas() {
        el("input#filtro-campus").click();
        waitUntilCliclableAndClick( el("#vs1__option-3"));
    }
    @Entao("^aparece o resultado com o campus Canoas$")
    public void apareceOResultadoComOCampusCanoas() {
        waitUntilDisplayed(el("#portfolio-pessoa-nome"));
        assertThat(el("p", containingText("Campus Canoas"))).isNotNull();
    }



    @Quando("^informo a Área Ciência da Computação$")
    public void informoAAreaCienciaDaComputacao() {
        el("input#filtro-area").click();
        waitUntilCliclableAndClick(el("#vs2__option-15"));
    }
    @Entao("^aparece o resultado com uma pessoa da Área de Ciência da Computação$")
    public void apareceOResultadoComUmaPessoaDaAreaDeCienciaDaComputacao() {
        waitUntilDisplayed(el("#portfolio-pessoa-nome"));
        assertThat(el("li", containingText("Ciência da Computação"))).isNotNull();
    }



    @Quando("^clico nas Áreas de atuação$")
    public void clicoNasAreasDeAtuacao() {
        el("#tab-portfolio-areas___BV_tab_button__").click();
    }
    @E("^seleciono um tema da núvem de palavras$")
    public void selecionoUmTemaDaNuvemDePalavras() {
        waitUntilDisplayed(el(".jqcloud-word"));
        waitUntilCliclableAndClick(el(".jqcloud-word").el("a").asList().first());
    }
    @Entao("^o sistema apresenta os resultados da busca para o tema selecionado$")
    public void oSistemaApresentaOsResultadosDaBuscaParaOTemaSelecionado() {
        assertThat(url()).contains("/busca");
    }




    @Quando("^clico em grupo de pesquisa do CNPq$")
    public void clicoEmGrupoDePesquisaDoCNPq() {
        el("#tab-portfolio-grupos___BV_tab_button__").click();
    }

    @E("^seleciono um grupo de pesquisa$")
    public void selecionoUmGrupoDePesquisa() {
        waitUntilDisplayed(el(".infra-resultados__lista"));
        el(".infra-resultados__lista").el("a").click();
    }
    @Entao("^o sistema apresenta os detalhes do grupo de pesquisa selecionado$")
    public void oSistemaApresentaOsDetalhesDoGrupoDePesquisaSelecionado() {
        assertThat(el("p", containingText("Coordenador(a) do Grupo"))).isNotNull();
    }


    @E("^solicito buscar pessoas$")
    public void solicitoBuscar() {
        el("#filtro-nome").submit();
    }
    @Entao("aparece uma mensagem informando que não foram encontrados resultados")
    public void apareceUmaMensagemInformandoQueNãoForamEncontradosResultados() {
        assertThat(el(".alert-info", containingText("A pesquisa não encontrou resultados para o(s) critério(s) informado(s)."))).isNotNull();
    }
}
