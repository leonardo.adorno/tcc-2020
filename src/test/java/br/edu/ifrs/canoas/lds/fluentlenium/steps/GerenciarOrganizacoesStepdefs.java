package br.edu.ifrs.canoas.lds.fluentlenium.steps;

import br.edu.ifrs.canoas.lds.fluentlenium.config.BaseTest;
import br.edu.ifrs.canoas.lds.fluentlenium.config.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.containingText;
import static org.fluentlenium.core.filter.FilterConstructor.withText;

public class GerenciarOrganizacoesStepdefs extends BaseTest{
	
	private static final String GERENCIAR_ORGANIZACOES = Constants.URL + "parceria/organizacoes";
	
	@Dado("^estou na página organizações$")
	public void estouNaPaginaOrganizacoes(){
         await().atMost(5, TimeUnit.SECONDS).until(el("p.autenticacao__usuario")).displayed();
		 goTo(GERENCIAR_ORGANIZACOES);
	     assertThat(window().title()).contains("Organizações");
	}
	
	@Quando("^clico em nova organização$")
	public void clicoEmNovaOrganizacao(){
		waitUntilCliclableAndClick(el("#botao-novo"));
	}
	
	@E("^informo os dados da nova organização$")
	public void informoOsDadosDaNovaOrganizacao() {
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("OrgTeste");
		waitUntilDisplayed(el("#nomeCurto"));
		el("#nomeCurto").fill().withText("OT");
		el("#vs3__combobox").click();
		waitUntilCliclableAndClick(el("#vs3__option-2"));
        el("#vs4__combobox").click();
        waitUntilCliclableAndClick(el("#vs4__option-2"));
	}
	
	@E("^clico em salvar$")
	public void clicoEmSalvar(){
		el("#btn-salvar").click();
	}
	
	@Entao("^aparece a nova organização criada na lista de organizações$")
	public void apareceANovaOrganizacaoCriadaNaListaDeOrganizacoes() {
		await().atMost(5, TimeUnit.SECONDS).until(el(".painel-interno")).present();
		assertThat(el(".painel-interno").text()).containsIgnoringCase("OrgTeste");
	}
	
	
	
	@Quando("^clico em uma organização$")
	public void clicoEmUmaOrganizacao(){
		waitUntilDisplayed(el(".resultado__titulo"));
        el(".resultado__titulo").el("a").click();
	}
	
	@E("^clico em editar$")
	public void clicoEmEditar(){
		waitUntilDisplayed(el("#btn-editar"));
		el("#btn-editar").click();
	}
	
	@E("^altero os dados da organização$")
	public void alteroOsDadosDaOrganizacao(){
		waitUntilDisplayed(el("#nome"));
		el("#nome").fill().withText("OrgTeste2");
		waitUntilDisplayed(el("#nomeCurto"));
		el("#nomeCurto").fill().withText("OT2");
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que a organização foi alterada$")
	public void oSistemaApresentaUmaMensagemInformandoQueAOrganizacaoFoiAlterada(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	@E("^clico em excluir$")
	public void clicoEmExcluir(){
		waitUntilDisplayed(el("#btn-excluir"));
		el("#btn-excluir").click();
	}
	
	@E("^confirmo a exclusão$")
	public void confirmoAExclusaoDaOrganizacao(){
		waitUntilCliclableAndClick(el(".modal-footer").el(".btn-secondary"));
	}
	
	@Entao("^o sistema apresenta uma mensagem informando que a organização foi excluida$")
	public void oSistemaApresentaUmaMensagemInformandoQueAOrganizacaoFoiExcluida(){
		assertThat(el(".toast-body")).isNotNull();
	}
	
	
	
	
	
	
}
