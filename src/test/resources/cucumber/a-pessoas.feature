# language: pt
#@skip_scenario
Funcionalidade: Visualizar Pessoas. Este caso de uso tem como objetivo permitir ao visitante acessar o currículo de pessoas
  (docentes e técnicos-administrativos) da instituição, bem como os temas trabalhados por eles e
  os grupos de pesquisa registrados no Diretório de Grupos de Pesquisa do CNPq. É possível visualizar
  um painel e exportar de indicadores de produção dos servidores considerando o estrato Qualis.

  Cenario: Eu como um visitante posso procurar por um servidor e visualizar seu currículo
    Dado estou na página do portfólio pessoas
    Quando clico em um resultado
    Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio

  Delineação do Cenário: Eu como um visitante posso filtrar resultados de servidores por nome
    Dado estou na página do portfólio pessoas
    Quando informo o nome da pessoa "<nome>"
    E solicito buscar
    E clico em um resultado
    Entao aparece o resultado com o nome da pessoa "<nome>"

    Exemplos:
      |nome|
      |Rodrigo|
	    |Noll|

  Cenario: Eu como um visitante posso filtrar resultados de servidores por campus
    Dado estou na página do portfólio pessoas
    Quando informo o campus Canoas
    E solicito buscar
    E clico em um resultado
    Entao aparece o resultado com o campus Canoas


  Cenario: Eu como um visitante posso filtrar resultados de servidores por Área
    Dado estou na página do portfólio pessoas
    Quando informo a Área Ciência da Computação
    E solicito buscar
    E clico em um resultado
    Entao aparece o resultado com uma pessoa da Área de Ciência da Computação


  Cenario: Eu como um visitante posso navegar pela núvem de palavras das pessoas
    Dado estou na página do portfólio pessoas
    Quando clico nas Áreas de atuação
    E seleciono um tema da núvem de palavras
    Entao o sistema apresenta os resultados da busca para o tema selecionado


  Cenario: Eu como um visitante posso visualizar um grupo de pesquisa do CNPq
    Dado estou na página do portfólio pessoas
    Quando clico em grupo de pesquisa do CNPq
    E seleciono um grupo de pesquisa
    Entao o sistema apresenta os detalhes do grupo de pesquisa selecionado

  Esquema do Cenário: Eu como um visitante posso filtrar resultados de servidores por nome e não encontrar resultados
    Dado estou na página do portfólio pessoas
    Quando informo o nome da pessoa "<nome>"
    E solicito buscar pessoas
    Entao aparece uma mensagem informando que não foram encontrados resultados

    Exemplos:
      | nome     |
      | blabla    |


