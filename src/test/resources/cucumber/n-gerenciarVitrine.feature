# language: pt
#@skip_scenario
Funcionalidade: Gerenciar Vitrine Tecnológica. Este caso de uso tem como objetivo 
		permitir ao administrador gerenciar produtos desenvolvidos em projetos de pesquisa, 
		ensino ou extensão na vitrine tecnológica (criar, alterar e excluir).


  Cenário: Eu como um administrador posso criar uma nova tecnologia
    Dado estou na página vitrine tecnológica
    Quando clico em nova tecnologia
    E informo os dados da nova tecnologia
    E clico em salvar
    Entao aparece a nova tecnologia criada na lista de tecnologias


  Cenario: Eu como um administrador posso editar uma tecnologia
  	Dado estou na página vitrine tecnológica
    E clico em nova tecnologia
    E informo os dados da nova tecnologia
    E clico em salvar
    E aparece a nova tecnologia criada na lista de tecnologias
    E estou na página vitrine tecnológica
    E clico em uma tecnologia
    Quando clico em editar
    E altero os dados da tecnologia
    E clico em salvar
    E o sistema apresenta uma mensagem informando que a tecnologia foi alterada
    E clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que a tecnologia foi excluida


  Cenario: Eu como um administrador posso excluir uma tecnologia
  	Dado estou na página vitrine tecnológica
    E clico em nova tecnologia
    E informo os dados da nova tecnologia
    E clico em salvar
    E aparece a nova tecnologia criada na lista de tecnologias
    E estou na página vitrine tecnológica
    E clico em uma tecnologia
    Quando clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que a tecnologia foi excluida