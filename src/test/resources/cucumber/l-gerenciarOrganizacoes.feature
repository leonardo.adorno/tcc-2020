# language: pt
#@skip_scenario
Funcionalidade: Gerenciar Organizações. Este caso de uso tem como objetivo permitir tanto ao 
  servidor quanto ao administrador gerenciar organizações públicas e privadas 
  (criar, alterar e excluir).


  Cenário: Eu como um servidor ou administrador posso criar uma nova organização
    Dado estou na página organizações
    Quando clico em nova organização
    E informo os dados da nova organização
    E clico em salvar
    Entao aparece a nova organização criada na lista de organizações



  Cenario: Eu como um servidor ou administrador posso editar uma organização
  	Dado estou na página organizações
    E clico em nova organização
    E informo os dados da nova organização
    E clico em salvar
    E aparece a nova organização criada na lista de organizações
    E estou na página organizações
    E clico em uma organização
    Quando clico em editar
    E altero os dados da organização
    E clico em salvar
    E o sistema apresenta uma mensagem informando que a organização foi alterada
    E clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que a organização foi excluida
 

  Cenario: Eu como um servidor ou administrador posso excluir uma organização
  	Dado estou na página organizações
    E clico em nova organização
    E informo os dados da nova organização
    E clico em salvar
    E aparece a nova organização criada na lista de organizações
    E estou na página organizações
    E clico em uma organização
    Quando clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que a organização foi excluida
    
    
    
    