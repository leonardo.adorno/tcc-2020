# language: pt
#@skip_scenario
Funcionalidade: Gerenciar Laboratórios. Este caso de uso tem como objetivo permitir ao 
		administrador gerenciar laboratórios da instituição, para serem utilizados 
		(criar, alterar e excluir).


  Cenário: Eu como um administrador posso criar um novo laboratório
    Dado estou na página laboratórios
    Quando clico em novo laboratório
    E informo os dados do novo laboratório
    E clico em salvar
    Entao aparece o novo laboratório criado na lista de laboratórios


  Cenario: Eu como um administrador posso editar um laboratório
  	Dado estou na página laboratórios
    E clico em novo laboratório
    E informo os dados do novo laboratório
    E clico em salvar
    E aparece o novo laboratório criado na lista de laboratórios
    E estou na página laboratórios
    E clico em um laboratório
    Quando clico em editar
    E altero os dados do laboratório
    E clico em salvar
    E o sistema apresenta uma mensagem informando que o laboratório foi alterado
    E clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que o laboratório foi excluido
 

  Cenario: Eu como um administrador posso excluir um laboratório
  	Dado estou na página laboratórios
    E clico em novo laboratório
    E informo os dados do novo laboratório
    E clico em salvar
    E aparece o novo laboratório criado na lista de laboratórios
    E estou na página laboratórios
    E clico em um laboratório
    Quando clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que o laboratório foi excluido