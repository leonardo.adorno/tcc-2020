# language: pt
#@skip_scenario
Funcionalidade: Visualizar Habitats de Inovação. Este caso de uso tem como objetivo 
		permitir ao visitante acessar os habitats de inovação do IFRS, espaços que estimulam o 
		compartilhamento de conhecimento, promovem experiências criativas e desenvolvem 
		empreendedorismo a partir do trabalho colaborativo de servidores, estudantes e agentes da 
		sociedade. É possível visualizar o site externo do laboratório, seu regulamento e sua agenda.

  Cenario: Eu como um visitante posso selecionar um habitat de inovação e visualizá-lo
    Dado estou na página do escritório de projetos habitats de inovação
    Quando clico em um habitat de inovação
    Entao aparece o habitat de inovação e um texto indicando os equipamentos presentes nele