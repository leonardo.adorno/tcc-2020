# language: pt
#@skip_scenario
Funcionalidade: Visualizar Fluxos e Normas. Este caso de uso tem como objetivo
		permitir ao visitante acessar fluxos e normas, que apresentam aos servidores 
		as leis, normas, processos e documentos necessários para estabelecimentos de 
		parcerias, prestação de serviços e captação de recursos, com a possibilidade 
		do uso de Fundação de Apoio.

  Cenario: Eu como um visitante posso procurar por um fluxo ou norma e visualizá-lo
    Dado estou na página do escritório de projetos fluxos e normas
    Quando clico em um fluxo ou norma
    Entao aparece o fluxo ou norma com a última atualização

  Delineação do Cenário: Eu como um visitante posso filtrar resultados de fomentos por nome
    Dado estou na página do escritório de projetos fluxos e normas
    Quando informo o nome do fluxo ou norma "<nome>"
    E solicito buscar fluxos ou normas
    E clico em um fluxo ou norma
    Entao aparece o resultado com o nome do fluxo ou norma "<nome>"

    Exemplos:
      |nome|
      |novofluxo|
      |teste|

@skip_scenario
  Cenario: Eu como um visitante posso visualizar o fluxo completo de um fluxo ou norma
    Dado estou na página do escritório de projetos fluxos e normas
    Quando clico em um fluxo ou norma
    E clico na opção completo
    Entao aparece o fluxo completo do fluxo ou norma selecionado

  Esquema do Cenário: Eu como um visitante posso filtrar resultados de fluxos ou normas por nome e não encontrar resultados
    Dado estou na página do escritório de projetos fluxos e normas
    Quando informo o nome do fluxo ou norma "<nome>"
    E solicito buscar fluxos ou normas
    Entao aparece uma mensagem informando que não foram encontrados resultados

    Exemplos:
      | nome     |
      | educação    |
