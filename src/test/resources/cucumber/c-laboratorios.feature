#  language: pt
#@skip_scenario
 Funcionalidade: Visualizar Laboratório. Este caso de uso tem como objetivo permitir ao visitante acessar
 	os laboratórios da instituição, para serem utilizados, bem como os habitats de
 	inovação. É possível visualizar outros laboratórios e habitats de inovação do
 	responsável pelo laboratório escolhido.

   Cenario: Eu como um visitante posso procurar por um laboratório e visualizá-lo
     Dado estou na página do portfólio laboratórios
     Quando clico em um resultado
     Entao aparece o laboratório e um texto indicando o responsável por ele

   Delineação do Cenário: Eu como um visitante posso filtrar resultados de laboratórios por nome
     Dado estou na página do portfólio laboratórios
     Quando informo o nome do laboratório "<nome>"
     E solicito buscar
     E clico em um resultado
     Entao aparece o resultado com o nome do laboratório "<nome>"

     Exemplos:
       |nome|
       |matemática|

   Cenario: Eu como um visitante posso filtrar resultados de laboratórios por campus
     Dado estou na página do portfólio laboratórios
     Quando informo o campus Canoas
     E solicito buscar
     E clico em um resultado
     Entao aparece o resultado com o laboratório do campus Canoas

   Cenario: Eu como um visitante posso procurar por um habitat de inovação e visualizá-lo
     Dado estou na página do portfólio laboratórios
     Quando clico em habitats de inovação
     E clico em um habitat
     Entao aparece o habitat de inovação e um texto indicando os equipamentos presentes nele


   Esquema do Cenário: Eu como um visitante posso filtrar resultados de laboratórios por nome e não encontrar resultados
     Dado estou na página do portfólio laboratórios
     Quando informo o nome do laboratório "<nome>"
     E solicito buscar laboratorios
     Entao aparece uma mensagem informando que não foram encontrados resultados

     Exemplos:
       | nome     |
       | criterio   |



