# language: pt
#@skip_scenario
Funcionalidade: Gerenciar Parcerias. Este caso de uso tem como objetivo permitir tanto ao 
		servidor quanto ao administrador gerenciar parcerias com organizações públicas e 
		privadas (criar, alterar e excluir).

  Cenário: Eu como um servidor ou administrador posso criar uma nova parceria
    Dado estou na página parcerias
    Quando clico em nova parceria
    E informo os dados da nova parceria
    E clico em salvar
    Entao aparece a nova parceria criada na lista de parcerias


  Cenario: Eu como um servidor ou administrador posso editar uma parceria
  	Dado estou na página parcerias
    E clico em nova parceria
    E informo os dados da nova parceria
    E clico em salvar
    E aparece a nova parceria criada na lista de parcerias
    E estou na página parcerias
    E clico em uma parceria
    Quando clico em editar
    E altero os dados da parceria
    E clico em salvar
    E o sistema apresenta uma mensagem informando que a parceria foi alterada
    E clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que a parceria foi excluida


  Cenario: Eu como um servidor ou administrador posso excluir uma parceria
  	Dado estou na página parcerias
    E clico em nova parceria
    E informo os dados da nova parceria
    E clico em salvar
    E aparece a nova parceria criada na lista de parcerias
    E estou na página parcerias
    E clico em uma parceria
    Quando clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que a parceria foi excluida