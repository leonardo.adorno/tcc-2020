# language: pt
#@skip_scenario
Funcionalidade: Gerenciar Fluxo e Normas. Este caso de uso tem como objetivo permitir 
		ao administrador gerenciar fluxos e normas, que apresentam aos servidores as leis, 
		normas, processos e documentos necessários para estabelecimentos de parcerias, 
		prestação de serviços e captação de recursos (criar, alterar e excluir).



  Cenário: Eu como um administrador posso criar um novo fluxo ou norma
    Dado estou na página fluxos e normas
    Quando clico em novo fluxo ou norma
    E informo os dados do novo fluxo ou norma
    E clico em salvar
    Entao aparece o novo fluxo ou norma criado na lista de fluxos e normas


  Cenario: Eu como um administrador posso editar um fluxo ou norma
  	Dado estou na página fluxos e normas
    E clico em novo fluxo ou norma
    E informo os dados do novo fluxo ou norma
    E clico em salvar
    E aparece o novo fluxo ou norma criado na lista de fluxos e normas
    E estou na página fluxos e normas
    E clico em um fluxoo ou normaa
    Quando clico em editar
    E altero os dados do fluxo ou norma
    E clico em salvar
    E o sistema apresenta uma mensagem informando que o fluxo ou norma foi alterado
    E clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que o fluxo ou norma foi excluido
 

  Cenario: Eu como um administrador posso excluir um fluxo ou norma
  	Dado estou na página fluxos e normas
    E clico em novo fluxo ou norma
    E informo os dados do novo fluxo ou norma
    E clico em salvar
    E aparece o novo fluxo ou norma criado na lista de fluxos e normas
    E estou na página fluxos e normas
    E clico em um fluxoo ou normaa
    Quando clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que o fluxo ou norma foi excluido