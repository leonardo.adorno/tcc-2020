# language: pt
#@skip_scenario
  Funcionalidade: Visualizar Prestação de Serviços. Este caso de uso tem como objetivo permitir ao visitante acessar oportunidades de prestação de serviços de laboratórios do IFRS
  	e de conhecimento, tais como consultorias, capacitações, para a solução de demandas de empresas públicas, privadas e pessoas físicas.
  
    Cenario: Eu como um visitante posso procurar por uma prestação de serviço de uma pessoa e visualizá-la
      Dado estou na página da prestação de serviços
      Quando clico em uma prestação de serviço de uma pessoa
      Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio


    Delineação do Cenário: Eu como um visitante posso filtrar resultados de pessoas que prestam serviços por critério
      Dado estou na página da prestação de serviços
      Quando clico nos filtros
      E informo o critério "<nome>"
      E solicito buscar prestação de serviços
      E clico em uma prestação de serviço de uma pessoa
      Entao aparece o resultado com a prestação de serviço "<nome>"
  
      Exemplos:
        |nome|
        |Rodrigo|

    Cenario: Eu como um visitante posso filtrar resultados de servidores por campus
      Dado estou na página da prestação de serviços
      Quando clico nos filtros
      E informo a reitoria
      E solicito buscar prestação de serviços
      E clico em uma prestação de serviço de uma pessoa
      Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio


    Cenario: Eu como um visitante posso procurar por uma prestação de serviço de um laboratório e visualizá-la
      Dado estou na página da prestação de serviços
      Quando clico em laboratórios
      E seleciono uma prestação de serviço de um laboratório
      Entao aparece o laboratório e um texto indicando que é prestador de serviço


    Delineação do Cenário: Eu como um visitante posso filtrar resultados de servidores por critério e não encontrar resultados
      Dado estou na página da prestação de serviços
      Quando clico nos filtros
      E informo o critério "<nome>"
      E solicito buscar prestação de serviços
      Entao aparece uma mensagem informando que não foram encontrados resultados
  
      Exemplos:
        | nome     |
        | Testando    |
        