 # language: pt
#@skip_scenario
 Funcionalidade: Visualizar Videoteca. Este caso de uso tem como objetivo permitir ao 
 		visitante acessar palestras, minicursos e transmissões sobre inovação, empreendedorismo, 
 		habitats de inovação, legislação, propriedade intelectual e novas tecnologias.


   Cenario: Eu como um visitante posso procurar por um vídeo sobre empreendedorismo e visualizá-lo
     Dado estou na página do escritório de projetos videoteca
     Quando clico em empreendedorismo
     E clico em um vídeo
     Entao aparece o vídeo e o seu autor

@skip_scenario
   Delineação do Cenário: Eu como um visitante posso filtrar resultados de vídeos por nome
     Dado estou na página do escritório de projetos videoteca
 		 Quando clico nos filtros
     E informo o nome do vídeo "<nome>"
     E solicito buscar vídeos
     E clico em um vídeo
     Entao aparece o resultado com o nome do vídeo "<nome>"

     Exemplos:
       |nome|
       |Gestão|


   Cenario: Eu como um visitante posso procurar por um vídeo sobre habitats de inovação e visualizá-lo
     Dado estou na página do escritório de projetos videoteca
     Quando clico em Habitats de Inovação
     E clico em um vídeo
     Entao aparece o vídeo e o seu autor
    

   Cenario: Eu como um visitante posso procurar por um vídeo sobre inovação e visualizá-lo
     Dado estou na página do escritório de projetos videoteca
     Quando clico em Inovação
     E clico em um vídeo
     Entao aparece o vídeo e o seu autor
    

   Cenario: Eu como um visitante posso procurar por um vídeo sobre legislação e visualizá-lo
     Dado estou na página do escritório de projetos videoteca
     Quando clico em Legislação
     E clico em um vídeo
     Entao aparece o vídeo e o seu autor
    

   Cenario: Eu como um visitante posso procurar por um vídeo sobre novas tecnologias e visualizá-lo
     Dado estou na página do escritório de projetos videoteca
     Quando clico em Novas Tecnologias
     E clico em um vídeo
     Entao aparece o vídeo e o seu autor
    

   Cenario: Eu como um visitante posso procurar por um vídeo sobre propriedade intelectual e visualizá-lo
     Dado estou na página do escritório de projetos videoteca
     Quando clico em Propriedade Intelectual
     E clico em um vídeo
     Entao aparece o vídeo e o seu autor


   Esquema do Cenário: Eu como um visitante posso filtrar resultados de vídeos por nome e não encontrar resultados
     Dado estou na página do escritório de projetos videoteca
     Quando clico nos filtros
     E informo o nome do vídeo "<nome>"
     E solicito buscar vídeos
     Entao aparece uma mensagem informando que não foram encontrados resultados

     Exemplos:
       | nome     |
       | seminário    |