# language: pt
#@skip_scenario
Funcionalidade: Visualizar Fomentos Externos. Este caso de uso tem como objetivo permitir ao 
		visitante acessar oportunidades de fomento para projetos de Pesquisa, Desenvolvimento e 
		Inovação (P,D&I) a partir de diversas fontes externas de financiamento, incluindo um 
		sistema de recomendação com base na área de atuação do servidor do IFRS.

  Cenario: Eu como um visitante posso procurar por um fomento externo vigente e visualizá-lo
    Dado estou na página do escritório de projetos fomentos externos
    Quando clico em um fomento externo vigente
    Entao aparece o fomento extereno vigente com a última atualização

  Delineação do Cenário: Eu como um visitante posso filtrar resultados de fomentos por nome
    Dado estou na página do escritório de projetos fomentos externos
    Quando informo o nome do fomento "<nome>"
    E solicito buscar fomentos
    E clico em um fomento externo vigente
    Entao aparece o resultado com o nome do fomento "<nome>"

    Exemplos:
      |nome|
      |prêmio|

  Cenario: Eu como um visitante posso filtrar resultados de fomentos por entidade financiadora
    Dado estou na página do escritório de projetos fomentos externos
    Quando informo a entidade financiadora CAPES
    E solicito buscar fomentos
    E clico em um fomento externo vigente
    Entao aparece o resultado com a entidade financiadora CAPES


  Cenario: Eu como um visitante posso filtrar resultados de fomentos por tipo
    Dado estou na página do escritório de projetos fomentos externos 
    Quando informo o tipo Concursos e Prêmios
    E solicito buscar fomentos
    E clico em um fomento externo vigente
    Entao aparece o resultado com o tipo Concursos e Prêmios

@skip_cenario
  Cenario: Eu como um visitante posso procurar por um fomento externo encerrado e visualizá-lo
    Dado estou na página do escritório de projetos fomentos externos
    Quando clico nos fomentos externos encerrados
    E clico em um fomento externo encerrado
    Entao aparece o fomento extereno encerrado com a última atualização


  Esquema do Cenário: Eu como um visitante posso filtrar resultados de fomentos externos por nome e não encontrar resultados
    Dado estou na página do escritório de projetos fomentos externos
    Quando informo o nome do fomento "<nome>"
    E solicito buscar fomentos
    Entao aparece uma mensagem informando que não foram encontrados resultados

    Exemplos:
      | nome     |
      | externo    |