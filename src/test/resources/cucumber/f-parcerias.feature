#   language: pt
#@skip_scenario
 Funcionalidade: Visualizar Organizações Parceiras. Este caso de uso tem como objetivo permitir ao visitante acessar organizações públicas e privadas, associações e fóruns interessados
 	no desenvolvimento socioeconômico sustentável que possuem parceria com o IFRS.
 

   Cenario: Eu como um visitante posso visualizar uma organização parceira
     Dado estou na página da organizações parceiras
     Quando clico em uma organização parceira
     Entao aparece a organização parceira selecionada vinculada com o extrato de publicação no DOU
 
 	@skip_scenario
   Delineação do Cenário: Eu como um visitante posso filtrar resultados de organizações parceiras por nome
     Dado estou na página da organizações parceiras
     Quando clico nos filtros
     E informo o nome da organização parceira "<nome>"
     E solicito buscar organizações parceiras
     E clico em uma organização parceira
     Entao aparece o resultado com o nome da organização parceira "<nome>"
 
     Exemplos:
       |nome|
       |tes|
 
 
   Esquema do Cenário: Eu como um visitante posso filtrar resultados de organizações parceiras por nome e não encontrar resultados
     Dado estou na página da organizações parceiras
     Quando clico nos filtros
     E informo o nome da organização parceira "<nome>"
     E solicito buscar organizações parceiras
     Entao aparece uma mensagem informando que não foram encontrados resultados
 
     Exemplos:
       | nome     |
       | organização  |
