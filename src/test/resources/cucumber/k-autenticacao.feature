# language: pt

Funcionalidade: O servidor do IFRS que acessa o site do Integra pode se autenticar e acessar
 funcionalidades específicas para o seu perfil.
 

  Cenario: Eu como um servidor posso me autenticar no Integra
    Dado estou na página inicial do Integra
    Quando clico em entrar no sistema
    E informo credenciais válidas
    Entao É apresentada a tela inicial com o usuário autenticado

