# language: pt
#@skip_scenario
 Funcionalidade: Visualizar Vitrine Tecnológica. Este caso de uso tem como objetivo permitir ao visitante acessar produtos desenvolvidos em projetos de pesquisa,
 	ensino ou extensão, protegidos ou não, e passíveis de transferência, licenciamento ou desenvolvimento conjunto com empresas
 	ou instituições interessadas. É possível visualizar o nível de maturidade tecnológica do projeto.

   Cenario: Eu como um visitante posso procurar por um produto ou serviço e visualizá-lo
     Dado estou na página da vitrine tecnológica
     Quando clico em um produto ou serviço
     Entao aparece o produto ou serviço e um texto indicando o processo no INPI


   Delineação do Cenário: Eu como um visitante posso filtrar resultados de produtos ou serviços por nome
     Dado estou na página da vitrine tecnológica
     Quando clico nos filtros
     E informo o nome do produto ou serviço "<nome>"
     E solicito buscar produtos ou serviços
     E clico em um produto ou serviço
     Entao aparece o resultado com o nome do produto ou serviço "<nome>"
 
     Exemplos:
       |nome|
       |teste|
 

   Cenario: Eu como um visitante posso filtrar resultados de produtos ou serviços por categoria
     Dado estou na página da vitrine tecnológica
     Quando clico nos filtros
     E informo a categoria engenharias
     E solicito buscar produtos ou serviços
     E clico em um produto ou serviço
     Entao aparece o resultado com a categoria engenharias
 

   Cenario: Eu como um visitante posso filtrar resultados de produtos ou serviços por propriedade intelectual
     Dado estou na página da vitrine tecnológica
     Quando clico nos filtros
     E informo a propriedade intelectual Software
     E solicito buscar produtos ou serviços
     E clico em um produto ou serviço
     Entao aparece o resultado com um produto ou serviço da propriedade intelectual Software
 

   Cenario: Eu como um visitante posso visualizar o nível de maturidade do projeto
     Dado estou na página da vitrine tecnológica
     Quando clico em um produto ou serviço
     E clico no nível de maturidade do projeto
     Entao o sistema apresenta o nível de maturidade tecnológica do projeto
 

   Esquema do Cenário: Eu como um visitante posso filtrar resultados de produtos ou serviços por nome e não encontrar resultados
     Dado estou na página da vitrine tecnológica
     Quando clico nos filtros
     E informo o nome do produto ou serviço "<nome>"
     E solicito buscar produtos ou serviços
     Entao aparece uma mensagem informando que não foram encontrados resultados
 
     Exemplos:
       | nome     |
       | Integra    |

