#  language: pt
#@skip_scenario
 Funcionalidade: Visualizar Produções. Este caso de uso tem como objetivo permitir ao visitante acessar
 		produções técnicas produzidas por pessoas (docentes e técnicos-administrativos) da instituição,
 		bem como produções bibliográficas, produções artísticas e culturais e projetos desenvolvidos por eles.

   Cenario: Eu como um visitante posso procurar por uma produção técnica e visualiza-lá
     Dado estou na página do portfólio produções
     Quando clico em um resultado
     Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio

   Delineação do Cenário: Eu como um visitante posso filtrar resultados de produções por nome
     Dado estou na página do portfólio produções
     Quando informo o nome da produção "<nome>"
     E solicito buscar
     E clico em um resultado
     Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio

     Exemplos:
       |nome|
       |seminário|
       |debate|

   Cenario: Eu como um visitante posso filtrar resultados de produções por campus
     Dado estou na página do portfólio produções
     Quando informo o campus Canoas
     E solicito buscar
     E clico em um resultado
     Entao aparece o resultado com o campus Canoas

   Cenario: Eu como um visitante posso procurar por uma produção bibliográfica e visualiza-lá
     Dado estou na página do portfólio produções
     Quando clico em produção bibliográfica
     E seleciono uma produção bibliográfica
     Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio

   Cenario: Eu como um visitante posso procurar por uma produção artística e cultural e visualiza-lá
     Dado estou na página do portfólio produções
     Quando clico em produção artística e cultural
     E seleciono uma produção artística e cultural
     Entao aparece o currículo do servidor e um texto indicando a data de atualização do portfólio

   Cenario: Eu como um visitante posso procurar por um projeto e visualiza-lo
     Dado estou na página do portfólio produções
     Quando clico em projetos
     E seleciono um projeto
     Entao aparece o projeto selecionado e os seus dados

   Esquema do Cenário: Eu como um visitante posso filtrar resultados de produções por nome e não encontrar resultados
     Dado estou na página do portfólio produções
     Quando informo o nome da produção "<nome>"
     E solicito buscar produções
     Entao aparece uma mensagem informando que não foram encontrados resultados

     Exemplos:
       | nome     |
       | producoes    |

