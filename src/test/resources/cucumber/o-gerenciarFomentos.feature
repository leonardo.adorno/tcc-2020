# language: pt
#@skip_scenario
Funcionalidade: Gerenciar Fomentos Externos. Este caso de uso tem como objetivo 
		permitir ao administrador gerenciar  fomentos para projetos de Pesquisa, 
		Desenvolvimento e Inovação (P,D&I) (criar, alterar e excluir).


  Cenário: Eu como um administrador posso criar um novo fomento
    Dado estou na página fomento externo
    Quando clico em notificar um fomento
    E informo os dados do novo fomento
    E clico em salvar
    Entao aparece o novo fomento criado na lista de fomentos

@skip_scenario
  Cenario: Eu como um administrador posso editar uma fomento
  	Dado estou na página fomento externo
    E clico em notificar um fomento
    E informo os dados do novo fomento
    E clico em salvar
    E aparece o novo fomento criado na lista de fomentos
    E estou na página fomento externo
    E clico em um fomento
    Quando clico em editar
    E altero os dados do fomento
    E clico em salvar
    E o sistema apresenta uma mensagem informando que o fomento foi alterado
    E clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que o fomento foi excluido

@skip_scenario
  Cenario: Eu como um administrador posso excluir um fomento
  	Dado estou na página fomento externo
    E clico em notificar um fomento
    E informo os dados do novo fomento
    E clico em salvar
    E aparece o novo fomento criado na lista de fomentos
    E estou na página fomento externo
    E clico em um fomento
    Quando clico em excluir
    E confirmo a exclusão
    Entao o sistema apresenta uma mensagem informando que o fomento foi excluido